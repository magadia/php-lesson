<?php 
	require "../templates/template.php";

	function get_content(){
?>
	<h1 class="text-center py-4">CATALOG</h1>

	<div class="container">
		<div class="row">
			<?php 

			$products = file_get_contents("../assets/lib/products.json");
			$products_arrray = json_decode($products, true);

			foreach($products_arrray as $inDiv_product){
			?>

			<div class="col-lg-4 py-2">
				<div class="card">
					<img class="card-img-top" height="450px" src="../assets/lib/<?php echo $inDiv_product['image'] ?>" alt="">
					<div class="card-body">
						<h5 class="card-title"><?php echo $inDiv_product['name'] ?></h5>
						<p class="card-text">Price: <?php echo $inDiv_product['price']; ?></p>
						<p class="card-text">Description: <?php echo $inDiv_product['description'] ?></p>
					</div>
					<?php  
						if (isset($_SESSION['email']) && $_SESSION['email']=="admin@admin.com") {
						?>
					<div class="card-footer">
						<a href="../controllers/process_delete_product.php?name=<?php echo $inDiv_product['name'] ?>" class="btn btn-danger">Delete item</a>
					</div>
						<?php
						}else{

						?>
					<div class="card-footer">
						<form action="../controllers/process_add_to_cart.php" method="POST">

							<input type="hidden" value="<?php echo $inDiv_product['name']?>" name='name'>

							<input type="number" name="quantity" value="1" class="form-control">

							<button type="submit" class="btn btn-info">Add to Cart</button>
						</form>
					</div>
					<?php 
				}
					 ?>
				</div>
			</div>


			<?php  
			}

			 ?>
		</div>	
	</div>

	<?php  
	
	}	

 ?>